---
name: Bug report
about: Bug report
title: "[Bug] "
labels: ''
assignees: ''

---

**Issue Feedback**

Note: Any posts that do not follow this template will be closed immediately. I won't be able to address your issue if the information below is not provided.

Please mark "x" inside the square brackets to indicate that you have read the following information.
- [ ] I confirm that I have searched for a solution to this issue in the [FAQ](https://physton.github.io/sd-webui-prompt-all-in-one-assets/#/FAQ) and couldn't find a solution.
- [ ] I confirm that I have searched for this issue in the [Issues](https://github.com/Physton/sd-webui-prompt-all-in-one/issues) list (including closed ones) and couldn't find a solution.
- [ ] I confirm that I have read the [Wiki](https://physton.github.io/sd-webui-prompt-all-in-one-assets/#/Installation) and couldn't find a solution.

**Describe the Issue**
Please describe the problem you encountered here.

**Steps to Reproduce**
Please let me know the steps you took to trigger the issue.

**Screenshots**
Please provide console screenshots or screenshots of the issue if possible.

**Some necessary information**
 - Stable Diffusion WebUI version: [e.g. b6af0a3, 1.3.1]
 - Extension version: [e.g. e0498a1]
 - OS: [e.g. windows, macos, linux]
 - Browser: [e.g. chrome, edge, safari]
 - Python version: [e.g. 3.10.11]
 - Gradio version: [e.g. 3.31.0]

**Additional Logs**
Add any relevant logs regarding the issue here.